FROM rocker/shiny:4.1.2
LABEL maintainer="christopher-grycki@uiowa.edu"

# Update package lists
RUN apt-get update -qq

# Install security updates
RUN apt-get -s dist-upgrade | grep "^Inst" | grep -i securi | awk -F " " {'print $2'} | xargs apt-get install

# Install dev packages required to build R packages
# RUN apt-get -y --no-install-recommends install \
#     libgdal-dev \
#     libudunits2-dev

# Install R packages required by this application
# RUN install2.r --error \
#     --deps TRUE \
#     --skipinstalled \
#     shinythemes \
#     shinyBS \
#     shinyjs \
#     MASS \
#     rgdal \
#     lubridate \
#     rgeos \
#     sf \
#     stringr \
#     lme4 

# cleanup downloaded R packages
# RUN rm -rf /tmp/downloaded_packages

# copy application code into the container
RUN rm -rf /srv/shiny-server
COPY --chown=shiny:shiny ./code /srv/shiny-server

# set healthcheck
HEALTHCHECK --interval=30s --timeout=30s --start-period=5s --retries=3 CMD wget -nv --spider --method=HEAD http://localhost:3838/ || exit 1

ARG BUILD_DATE
ARG VCS_REF
ARG VCS_URL

# Labels
LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.build-date=${BUILD_DATE}
LABEL org.label-schema.vcs_ref=${VCS_REF}
LABEL org.label-schema.vcs_url=${VCS_URL}

USER shiny